Mutliples est destiné à représenter les multiplications sous forme de quadrillages.

<a href="https://forge.apps.education.fr/educajou/multiples/-/raw/main/paquets/multiples_1_all.deb" target="_blank">
<span class="bouton">
Télécharger Multiples pour Linux (DEB)
</span>
</a>

<a href="https://forge.apps.education.fr/educajou/multiples/-/raw/main/paquets/Multiples_installeur_v1.exe" target="_blank">
<span class="bouton">
Télécharger Multiples pour Windows (10 et +)
</span>
</a>

![Capture d'écran exemple](https://forge.apps.education.fr/educajou/multiples/-/raw/main/images/screenshot.png "Exemple d'utilisation")


