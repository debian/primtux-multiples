#!/bin/sh
fpm -n multiples \
-s dir \
-t deb \
--depends python-tk \
--depends python3-tk \
--name multiples \
--license GPL \
--version 1 \
--architecture all \
--description "Mutliples est destiné à représenter les multiplications sous forme de quadrillages." \
--url "https://forge.apps.education.fr/educajou/tuxblocs]https://forge.apps.education.fr/educajou/multiples" \
--maintainer "Arnaud Champollion" \
linux/multiples.desktop=/usr/share/applications/multiples.desktop \
multiples.py=/opt/multiples/multiples.py \
images=/opt/multiples
