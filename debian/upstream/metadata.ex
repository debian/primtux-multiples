# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/primtux-multiples/issues
# Bug-Submit: https://github.com/<user>/primtux-multiples/issues/new
# Changelog: https://github.com/<user>/primtux-multiples/blob/master/CHANGES
# Documentation: https://github.com/<user>/primtux-multiples/wiki
# Repository-Browse: https://github.com/<user>/primtux-multiples
# Repository: https://github.com/<user>/primtux-multiples.git
